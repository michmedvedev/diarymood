import * as React from 'react'
import {DefaultTheme, NavigationContainer} from '@react-navigation/native'
import {createNativeStackNavigator} from '@react-navigation/native-stack'
import {Home} from '../screens/Home'
import {Settings} from '../screens/Settings'

DefaultTheme.colors.background = '#fff';
const Stack = createNativeStackNavigator();

export const RootNav = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={Home}
          options={{title: 'Дневник'}}
        />
        <Stack.Screen 
          name="Settings"
          component={Settings}
          options={{title: 'Заполнить дневник'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};