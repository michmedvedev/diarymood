import React from 'react';
import {RootNav} from "./navigation"
import {store} from "./store"
import {Provider} from 'react-redux';

const App = () => {
  return (
    <Provider store={store}>
      <RootNav />
    </Provider>
  );
};

export default App;
