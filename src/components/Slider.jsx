import React from "react";
import { SafeAreaView, View, Text, FlatList, StyleSheet, Dimensions } from "react-native";
import MultiSlider from "@ptomasroos/react-native-multi-slider"
import {fonts, colors} from '../config'

export const CustomeSlider = ({position, setPosition, title, label, labelColor}) => {
  return(
    <View style={styles.container}>
      <Text style={styles.title}>{title || "Уровень стресса"}</Text>
      <Text style={[styles.subTitle, {color: labelColor[position]}]}>{label[position]}</Text>
      <MultiSlider
        values={[position]}
        min={0}
        max={4}
        step={1}
        sliderLength={340}
        selectedStyle={{
          // backgroundColor: labelColor[position - 1],
        }}
        unselectedStyle={{
          // backgroundColor: labelColor[position],
        }}
        containerStyle={{
          width: Dimensions.get('screen').width
        }}
        trackStyle={{
          backgroundColor: labelColor[position],
        }}
        markerStyle={{
          width: 26,
          height: 26,
          backgroundColor: labelColor[position]
        }}
        touchDimensions={{
          height: 40,
          width: 40,
          borderRadius: 20,
          slipDisplacement: 40,
        }}
        // imageBackgroundSource={{uri: "https://reactjs.org/logo-og.png"}}
        markerOffsetX={11}
        showSteps={true}
        showStepMarkers={true}
        showStepLabels={true}
        snapped={true}
        onValuesChange={item => setPosition(item[0])}
        
      />
    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    marginTop: 15,
  },
  title: {
    fontSize: 16,
    color: colors.TITLE,
    fontFamily: fonts.REGULAR,
  },
  subTitle: {
    fontSize: 16,
    color: colors.SUBTITLE,
    fontFamily: fonts.REGULAR,
  }
});
