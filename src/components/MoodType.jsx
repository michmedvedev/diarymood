import React from "react";
import { SafeAreaView, View, Text, Image, StyleSheet, Dimensions } from 'react-native';
import {colors, fonts, label, labelColor} from '../config'

export const MoodType = ({title, val}) => {
  return(
    <View style={styles.container}>
      <View style={styles.row}>
        <View style={[styles.point, {backgroundColor: labelColor[val] || "orange"}]} />
        <Text style={styles.title}>{title || ""}</Text>
      </View>
      <Text style={styles.subTitle}>{label[val] || ""}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 11
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: "flex-start",
  },
  point: {
    borderRadius: 52,
    width: 10,
    height: 10
  },
  title: {
    marginLeft: 8,
    fontSize: 14,
    color: colors.TITLE,
    fontFamily: fonts.MEDIUM,
  },
  subTitle: {
    marginLeft: 18,
    fontSize: 10,
    color: colors.BLACK,
    fontFamily: fonts.REGULAR,
  }
})