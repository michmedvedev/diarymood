import React from "react";
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from "react-redux";
import { SafeAreaView, View, TouchableOpacity, Text, Image, FlatList, StyleSheet, Dimensions } from 'react-native';
import {fonts, colors, labelMood, DATA} from '../config'
import {MoodType} from "../components/MoodType"
import {moodActions} from '../store/moodReducer'

export const Home = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch()
  const {moodType, anxietyLevel, stressLevel} = useSelector(state => state);

  const setMoodType = (type) => {
    navigation.navigate('Settings')
    dispatch(moodActions.setMoodType(type.id))
  }

  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.title}>Настроение</Text>
      {moodType === 0 ? (
        <FlatList
          data={DATA}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          renderItem={({ item }) => (
            <TouchableOpacity style={styles.wrap} onPress={() => setMoodType(item)}>
              <Image source={item.uri} />
              <Text style={styles.titleItem}>{item.title}</Text>
            </TouchableOpacity>
          )}
          keyExtractor={item => item.id}
        />
      ) : (
        <View style={styles.row}>
          <View style={styles.firstColumn}>
            <View>
              <Image style={styles.img} source={DATA[moodType].uri} />
              <Image style={styles.check} source={require("../assets/diary_сheck.png")} />
            </View>
          <Text style={styles.subTitle}>{labelMood[moodType]}</Text>
          </View>
          <View style={styles.secondColumn}>
            <MoodType title="Тревожность" val={anxietyLevel} />
            <MoodType title="Стресс" val={stressLevel} />
          </View>
          <View style={styles.thirdColumn}>
            <TouchableOpacity onPress={() => navigation.navigate('Settings')}>
              <Image source={require("../assets/edit.png")} />
            </TouchableOpacity>
          </View>
        </View>
      )}
    </SafeAreaView>
  )
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    paddingHorizontal: 14,
  },
  row: {
    marginTop: 16,
    flexDirection: 'row',
    justifyContent: "space-between"
  },
  firstColumn: {
    flex: 0.3,
    borderRightWidth: 1,
    borderColor: '#EEEEEE',
    alignItems: 'center',
    paddingRight: 10,
    paddingTop: 10
  },
  secondColumn: {
    flex: 0.6,
  },
  thirdColumn: {},
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginRight: 16,
  },
  titleItem: {
    fontSize: 10,
    color: colors.BLACK,
    fontFamily: fonts.REGULAR,
    maxWidth: 100,
  },
  wrap: {
    marginVertical: 8,
    marginRight: 16,
  },
  title: {
    marginTop: 28,
    marginBottom: 16,
    fontSize: 16,
    color: colors.TITLE,
    fontFamily: fonts.MEDIUM,
  },
  subTitle: {
    marginTop: 3,
    fontSize: 10,
    color: colors.BLACK,
    fontFamily: fonts.REGULAR,
  },
  img: {
    marginTop: -10,
    width: 60,
    height: 60,
    backgroundColor: colors.BACKGROUND,
    borderRadius: 52
  },
  check: {
    position: 'absolute',
    bottom: 1,
    right: 0
  }
});
