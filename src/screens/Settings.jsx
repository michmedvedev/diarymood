import React, {useState, useEffect} from "react";
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from "react-redux";
import { View, Text, TouchableOpacity, FlatList, Image, StyleSheet, Dimensions } from 'react-native';
import {CustomeSlider} from '../components/Slider'
import {moodActions} from '../store/moodReducer'
import {fonts, colors, label, labelMood, labelColor, DATA} from '../config'

export const Settings = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch()
  const {moodType, anxietyLevel, stressLevel} = useSelector(state => state);
  const [anxiety, setAnxietyLocal] = useState(anxietyLevel)
  const [stress, setStressLocal] = useState(stressLevel)
  const [mood, setMoodLocal] = useState(moodType)
  const [disabled, setDisabled] = useState(false)

  useEffect(() => {
    if(stress !== stressLevel || anxiety !== anxietyLevel || mood !== moodType) {
      setDisabled(false)
    } else {
      setDisabled(true)
    }
  }, [stress, anxiety, mood])

  const save = () => {
    dispatch(moodActions.setAnxietyLevel(anxiety))
    dispatch(moodActions.setStressLevel(stress))
    dispatch(moodActions.setMoodType(mood))
    navigation.navigate("Home")
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Настроение</Text>
      <View>
        <FlatList
          data={DATA}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          renderItem={({ item }) => (
            <View style={{justifyContent: 'flex-start'}}>
              <TouchableOpacity onPress={() => setMoodLocal(item.id)} style={[styles.item, {backgroundColor: item.id === mood ? colors.BACKGROUND : 'transparent'}]}>
                <Image style={styles.img} source={item.uri} />
              </TouchableOpacity>
              <Text style={styles.itemText}>{labelMood[item.id]}</Text>
            </View>
          )}
          keyExtractor={item => item.id}
        />
      </View>
      <CustomeSlider
        position={anxiety || 0}
        setPosition={(val) => setAnxietyLocal(val)}
        title={"Уровень тревожности"}
        label={label}
        labelColor={labelColor}
      />
      <View style={styles.devider} />
      <CustomeSlider
        position={stress || 0}
        setPosition={(val) => setStressLocal(val)}
        title={"Уровень стресса"}
        label={label}
        labelColor={labelColor}
      />
      <TouchableOpacity
        disabled={disabled}
        style={[styles.btn, {backgroundColor: disabled ? colors.DISABLED : colors.BUTTON}]}
        onPress={() => save()}
      >
        <Text style={styles.btnText}>Сохранить</Text>
      </TouchableOpacity>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 14,
    paddingBottom: 8
  },
  item: {
    marginVertical: 8,
    marginRight: 16,
    borderRadius: 52
  },
  titleItem: {
    fontSize: 24,
  },
  title: {
    marginTop: 28,
    marginBottom: 16,
    fontSize: 16,
    color: colors.TITLE,
    fontFamily: fonts.MEDIUM,
  },
  devider: {
    marginTop: 25,
    width: Dimensions.get('screen').width,
    height: 1,
    backgroundColor: "#EEEEEE"
  },
  btn: {
    width: "100%",
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
    position: 'absolute',
    bottom: 10,
    left: 14
  },
  btnText: {
    fontSize: 16,
    color: colors.WHITE,
    fontFamily: fonts.MEDIUM,
  },
  itemText: {
    fontSize: 10,
    color: colors.BLACK,
    fontFamily: fonts.REGULAR,
    maxWidth: 100,
  }
});
