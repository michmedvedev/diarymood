export const DATA = [
  {
    id: 0,
    title: 'Радостное',
    uri: require("../assets/faces/0.png")
  },
  {
    id: 1,
    title: 'Спокойное',
    uri: require("../assets/faces/1.png")
  },
  {
    id: 2,
    title: 'Перепады настроения',
    uri: require("../assets/faces/2.png")
  },
  {
    id: 3,
    title: 'Грустное',
    uri: require("../assets/faces/3.png")
  },
  {
    id: 4,
    title: 'Подавленное',
    uri: require("../assets/faces/4.png")
  }
];

export const fonts = {
  REGULAR: 'Montserrat-Regular',
  MEDIUM: 'Montserrat-Medium',
};

export const colors = {
  TITLE: '#454A52',
  SUBTITLE: '#A1A9B5',
  BLACK: '#000000',
  WHITE: '#ffffff',
  BUTTON: '#0684F8',
  DISABLED: '#A1A9B5',
  BACKGROUND: '#FF558B59',
};

export const labelMood = ["Радостное", "Спокойное", "Перепады настроения", "Грустное", "Подавленное"]
export const label = ["отсутсвует", "легкий", "средний", "сильный", "невыносимый"]
export const labelColor = ["#A1A9B5", "#FFCC48", "#FFA73F", "#FC7E56", "#FC5656"]