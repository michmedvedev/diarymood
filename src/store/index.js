import {createStore} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';

import {moodReducer as mood} from './moodReducer';

const composeEnhancers = composeWithDevTools({
  // Specify here name, actionsBlacklist, actionsCreators and other options
});

export const store = createStore(mood, composeEnhancers());

export default store;
