const initialState = {
  moodType: 0,
  anxietyLevel: 0,
  stressLevel: 0,
}

const SET_ANXIETY_LEVEL = 'SET_ANXIETY_LEVEL';
const SET_STRESS_LEVEL = 'SET_STRESS_LEVEL';
const SET_MOOD_TYPE = 'SET_MOOD_TYPE';

export const moodActions = {
  setMoodType: (type) => ({type: SET_MOOD_TYPE, payload: type}),
  setAnxietyLevel: (level) => ({type: SET_ANXIETY_LEVEL, payload: level}),
  setStressLevel: (level) => ({type: SET_STRESS_LEVEL, payload: level}),
}

export const moodReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_MOOD_TYPE:
      return {...state, moodType: action.payload}
    case SET_ANXIETY_LEVEL:
      return {...state, anxietyLevel: action.payload}
    case SET_STRESS_LEVEL:
      return {...state, stressLevel: action.payload}
    default:
      return state;
  }
}